
public class SingletonlCalculator {
	private static SingletonlCalculator instance = null;

	private SingletonlCalculator() {
	}

	public int add(int i, int j, int k) {
		int result = i + j + k;
		return result;
	}

	public static SingletonlCalculator getInstance() {
		// TODO Auto-generated method stub
		if (instance == null) {
			instance = new SingletonlCalculator();
		}
		return instance;
	}

}
