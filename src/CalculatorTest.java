import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class CalculatorTest {

	@Disabled
	void testInitialCalculator() {
		Calculator c1 = new Calculator();
		// System.out.println(c1.add(4, 5, 6));
		assertEquals(19, c1.add(4, 5, 10));
	}

	@Test
	void testInitiaSingletonlCalculator() {
		SingletonlCalculator c2 = SingletonlCalculator.getInstance();
		assertEquals(15, c2.add(4, 5, 6));
	}

	// TODO Auto-generated method stub

}
